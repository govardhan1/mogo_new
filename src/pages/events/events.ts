import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { EventPage } from '../event/event';
import { CreatePage } from '../create/create';


/**
 * Generated class for the EventsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-events',
  templateUrl: 'events.html',
})
export class EventsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventsPage');
  }

  openEventPage(){
    this.navCtrl.push(EventPage);
  }

  openCreatePage(){
    this.navCtrl.push(CreatePage);
  }
  

}