import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { EventsPage } from '../events/events';
import { ChatPage } from '../chat/chat';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = AboutPage;
  tab2Root = EventsPage;
  tab3Root = HomePage;
  tab4Root = ChatPage;
  tab5Root = ContactPage;
  
  

  constructor() {

  }

}
